import React from 'react';
import './App.css';
class Button extends React.Component {
  render() {
    return (
      (this.props.disabled != this.props.name) ? (<button className="button" onClick={this.props.handleClick}>{this.props.name}</button>) : (<button className="disableButton">{this.props.name}</button>)
    );
}
}

export default Button;