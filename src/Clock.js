import React from 'react';
import './App.css';
class Clock extends React.Component {
    state = { date: new Date() };
    componentDidMount() {
        if (this.state.date) {
            this.timerID = setInterval(() => this.tick(), 1000);
        }
    }
    componentWillUnmount() {
        clearInterval(this.timerID);
    }
    tick = () => {
        this.setState({ date: new Date() });
    }
    render() {
        return (
            <h4 className="toppings">{this.state.date.toLocaleString()}</h4>
        );
    }
}

export default Clock;