import React, { Component } from 'react';
import './App.css';
import Button from './Button';
import globe from './globe.gif';
import Clock from './Clock';
import NEWS from './NEWS';
import PopupBlock from './PopupBlock';
class App extends Component {
  state = { isLoaded: false, data: [], countryName: null, buttonClicked: false };

  getNews = (country, con) => {
    this.setState({ isLoaded: false, buttonClicked: true });
    fetch(`https://newsapi.org/v2/top-headlines?source=google-news&country=${con}&apiKey=84660b17d11f4b9297592b7ec6586ec5`)
      .then(Response => Response.json())
      .then((json) => {
        this.setState({
          isLoaded: true,
          data: json,
          countryName: country.toUpperCase(),
          showImage: false,
          popData: null
        });
      })
  }

  popupImage = (image) => {
    this.setState({
      showImage: true,
      popData: image
    });
  }

  Reclicked = () => {
    this.setState({ showImage: false });
  }

  render() {
    var countryList = [
      { name: "india", code: "in" },
      { name: "china", code: "cn" },
      { name: "america", code: "us" },
      { name: "russia", code: "ru" },
      { name: "japan", code: "jp" },
      { name: "germany", code: "de" }
    ];
    window.onkeydown = (e) => {
      if (e.keyCode == 27) {
        this.Reclicked();
      }
    }
    return (
      <div>
        {this.state.showImage ? <PopupBlock data={this.state.popData} reclicked={this.Reclicked} /> : <div></div>}
        <div className="Head">
          <div className="logo">
            <img src={globe} alt='Globe' className="Globe" />
          </div>
          <div>
            <Clock />
            {<br></br>}
            {<h2 className="toppings">Welcome to Global NEWS ! Choose a country to veiw it's headlines.</h2>}
          </div>
          {countryList.map((country, i) => (<Button key={i} disabled={this.state.countryName} name={country.name.toUpperCase()} handleClick={() => this.getNews(country.name, country.code)} />))}
        </div>
        <div className="Body">
          <div className="Heading">{this.state.countryName}</div>
          <div>
            <NEWS news={this.state} imageClicked={this.popupImage} />
          </div>
        </div>
      </div>
    );
  }
}

export default App;
