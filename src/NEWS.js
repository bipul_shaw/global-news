import React from 'react';
import loader from './loader.gif';
import './App.css';
class NEWS extends React.Component {


    render() {
        return (
            this.props.news.isLoaded ? ((this.props.news.data.articles).map(item => (
                <div key={item.url} className="ContentBox">
                    <img src={item.urlToImage} className="Image" onClick={()=>this.props.imageClicked(item.urlToImage)} alt="NEWS"/>
                    <p>{item.description}</p>
                </div>
            ))) : (this.props.news.buttonClicked ? <div className="Loader"><img src={loader} alt='Loading...'/></div>:<div></div>)
        );
    }
}

export default NEWS;