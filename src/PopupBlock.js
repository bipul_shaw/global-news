import React from 'react';
import './App.css';
class PopupBlock extends React.Component {
  render() {
    return (
       <div className="BigImage" onClick={this.props.reclicked}>
           <img className="popImage" src={this.props.data} alt="Picture"/>
           <button className="popoff" onClick={this.props.reclicked}>X</button>
       </div>
      );
    }
  }
  
  export default PopupBlock;